import pandas as pd
import numpy as np
from matplotlib import pyplot as plt

# this gets the worldwide confirmed cases from JHU
file = 'time_series_covid19_deaths_global.csv'
url = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/'

# read/download csv
df = pd.read_csv(url + file)

# remove unnecessary columns
df.drop(['Province/State', 'Lat','Long'], axis=1, inplace=True)
corona_deaths = df.groupby(['Country/Region']).sum()
#save for a rainy day
corona_deaths.to_csv("corona_deaths.csv")
#corona_deaths.describe

# deaths are already cumulative, so all values after 
# the first non-zero value are returned
def get_only_nonzero(rowSeries, cols):
    nonzeros = rowSeries.to_numpy().nonzero()
    if nonzeros[0].size==0:
        return np.zeros(1)
    else:         
        return rowSeries.values[nonzeros[0][0]:cols + 1]

# get values as series
def get_fixed_series(country, cols):
    newrow = get_only_nonzero(corona_deaths.loc[country], cols)
    return pd.Series(newrow, name=country)

# Plots
# The first 3, 4 weeks comparing cumulative numbers is easier
# later maybe numbers by week: daily numbers fluctuate too much

# compare europe & US
get_fixed_series('France',cols).plot()
get_fixed_series('Germany',cols).plot()
get_fixed_series('Netherlands',cols).plot()
get_fixed_series('Italy',cols).plot()
get_fixed_series('Spain',cols).plot()
get_fixed_series('US',cols).plot()
plt.legend(loc="upper left")
plt.show()

# northern europe
get_fixed_series('Germany',cols).plot()
get_fixed_series('Netherlands',cols).plot()
get_fixed_series('Norway',cols).plot()
get_fixed_series('Sweden',cols).plot()
get_fixed_series('Denmark',cols).plot()
get_fixed_series('United Kingdom',cols).plot()
plt.legend(loc="upper left")
plt.show()

# plot asia + croatia, you'll see why
get_fixed_series('Japan',cols).plot()
get_fixed_series('Korea, South',cols).plot()
get_fixed_series('Singapore',cols).plot()
get_fixed_series('Taiwan*',cols).plot()
get_fixed_series('Croatia',cols).plot()
plt.legend(loc="upper left")
plt.show()

# compare
get_fixed_series('Japan',cols).plot()
get_fixed_series('Korea, South',cols).plot()
get_fixed_series('France',cols).plot()
get_fixed_series('China',cols).plot()
get_fixed_series('Netherlands',cols).plot()
get_fixed_series('Italy',cols).plot()
get_fixed_series('Spain',cols).plot()
plt.legend(loc="upper left")
plt.show()

# Others parts of the world were hit a little later
get_fixed_series('Argentina',cols).plot()
get_fixed_series('Brazil',cols).plot()
get_fixed_series('Ecuador',cols).plot()
get_fixed_series('Nigeria',cols).plot()
get_fixed_series('India',cols).plot()
plt.legend(loc="upper left")
plt.show()